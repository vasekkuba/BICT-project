library IEEE;
use IEEE.std_logic_1164.ALL;
use ieee.std_logic_unsigned.ALL;

entity pseudorng is
Port ( CLK : in std_logic;
       LFSR : out std_logic_vector (3 downto 0);
       LED: out std_logic_vector(3 downto 0));

end pseudorng;

architecture Behavioral of pseudorng is

    signal lfsr_temp: std_logic_vector(3 downto 0) := "1111";
    signal clk_250: std_logic := '0';
    signal tmp_250: std_logic_vector(11 downto 0) := x"000";
    signal tmp: std_logic := '0';

begin
    process (CLK)
    begin
        if rising_edge(CLK) then        --definice zpomaleneho signalu, aby bylo videt okem
            tmp_250 <= tmp_250 + 1;
            if tmp_250 = x"4e2" then
                tmp_250 <= x"000";
                clk_250 <= not clk_250;
            end if;
        end if;
    end process;

    process (CLK)
    begin
        if rising_edge(CLK) then                            --na nabeznou hranu urcim hodnotu xoru ze 2 bitu
            tmp <= lfsr_temp(1) xor lfsr_temp(0);
            lfsr_temp(2 downto 0) <= lfsr_temp(3 downto 1);             --posun bitovych pozic o jednu doprava  
            
        end if;
    end process;
    lfsr_temp(3) <= tmp;        -- prirazeni hodnoty z xoru na vstupni bitovou pozici, nahrani do LFSR pro simulaci a rozsviceni LED
    LFSR <= lfsr_temp;
    LED <= not lfsr_temp;       
end Behavioral;

