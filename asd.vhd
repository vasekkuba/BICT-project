library IEEE;
use IEEE.std_logic_1164.ALL;

entity tb_pseudorng is
end tb_pseudorng;

architecture bench of tb_pseudorng is

COMPONENT pseudorng
      Port ( CLK : in std_logic;
      LFSR : out std_logic_vector (3 downto 0));
     
END COMPONENT;

signal clk :  std_logic;
signal lfsr1: std_logic_vector(3 downto 0);

begin

mapping: pseudorng port map(
CLK => clk,
LFSR => lfsr1);

clock: process
begin
   clk <= '0'; wait for 50 ns;
   clk   <= '1'; wait for 50 ns;
end process;


end bench;